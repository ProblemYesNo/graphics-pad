#ifndef ME_GL_WINDOW_H
#define ME_GL_WINDOW_H

#include <string>
#include <GL\glew.h>
#include <QtOpenGL\QGLWidget>

class MeGlWindow : public QGLWidget
{
private:
protected:
	void initializeGL();
	void paintGL();
public:
	void sendDataToOpenGL();
	void installShaders();
};

#endif //ME_GL_WINDOW_H